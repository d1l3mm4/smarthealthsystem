# Sistema di Gestione Studio Medico

Sistema per la gestione dello studio di un medico di base

## Requirements
Per l'applicazione
 - JRE
 - Hibernate
 - MySQL Java Connector
 - Twilio-7.9.1-with-dependencies.jar
 
 Per il DBMS
 - MySQL
 
Progetto creato per Eclipse 2020-06
Tutte le dipendenze sono presenti nella directory `SmartHealthSystem/lib/`

## Setup DBMS
Per il primo avvio, bisogna popolare il database utilizzando il codice presente in `mysql.sql` per la creazione delle tabelle e delle relazioni.
Fatto ciò, creare un nuovo utente ed abilitarne l'accesso da una macchina remota in caso il database server non si trovi sulla stessa macchina dell'application server.
Questo può essere fatto modificando nel file `/etc/mysql/mysql.conf.d/mysqld.cnf`la riga:

    bind-address = <indirizzo-ip-pubblico | 0.0.0.0>

Nel caso si abbia a disposizione un indirizzo ip statico per l'inerfaccia che deve essere raggiungibile, inserire quello; altrimenti usare 0.0.0.0 per abilitare l'ascolto su tutte le interfacce
Per controllarne gli accessi inoltre, creare un nuovo utente in mysql:
Accedi alla console di mysql con

    mysql -u root -p

E dall'interprete sql inserire

    CREATE USER '<newuser>'@'%' IDENTIFIED BY '<user_password>';
Il carattere `%` permette all'utente di effettuare l'accesso da qualsiasi indirizzo.
Inoltre garantire a tale utente i permessi necessari all'utilizzo dell'applicazione.

## Installazione e primo avvio
Una volta effettuato il setup del dbms, modificare il file di configurazione di hibernate `SmartHealthSystem/scr/hibernate.cfg.xml`ed inserire qui le informazioni necessarie alla connessione:

    ...
    <!-- Driver per la connessione -->
    <property  name="connection.driver_class">com.mysql.cj.jdbc.Driver</property>
    <!-- Connection URL-->
    <property  name="connection.url">jdbc:mysql://IP:PORT/DATABASE</property> 
    <!-- Credenziali DBMS -->
    <property  name="connection.username">USERNAME</property>
    <property  name="connection.password">PASSWORD</property>
    ...
    <!-- Dialetto SQL -->
    <property  name="dialect">org.hibernate.dialect.MySQLDialect</property>
    ...

Fatto ciò, basta effettuare il build del progetto da Eclipse ed avviare
 - `Server.avvio` per avviare il processo server che fornisce l'interfaccia di Java RMI;
 - `Server.Boundary.Start` per avviare il client del Medico;
 - `ClientPaziente.Boundary.PazienteUI`per avviare il client del Paziente.
