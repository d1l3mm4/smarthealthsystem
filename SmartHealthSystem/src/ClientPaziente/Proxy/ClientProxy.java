package ClientPaziente.Proxy;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import ProxyRMInterface.LoginProxy;
import ProxyRMInterface.PrenotazioneProxy;
import ProxyRMInterface.PrescrizioneProxy;

import java.util.List;
import java.sql.Timestamp;

public class ClientProxy {

	public List<Timestamp> richiediVisita(String CF, boolean scelta, int page, Timestamp time) {

		try {
			Registry registry = LocateRegistry.getRegistry("localhost");
			PrenotazioneProxy proxy = (PrenotazioneProxy) registry.lookup("prenotazione");
			return proxy.richiediVisita(CF, scelta, page, time);
		}
		catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		
	}
	
	public boolean login(String CF, String password) {

		try {
			Registry registry = LocateRegistry.getRegistry("localhost");
			LoginProxy proxy = (LoginProxy) registry.lookup("login");
			return proxy.login(CF, password);
		}
		catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		
	}
	
	
	
	public String richiediPrescrizione(String CF) {

		try {
			Registry registry = LocateRegistry.getRegistry("localhost");
			PrescrizioneProxy proxy = (PrescrizioneProxy) registry.lookup("prescrizione");
			return proxy.richiediPrescrizione(CF);
		}
		catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		
	}

}