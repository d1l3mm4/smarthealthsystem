package ClientPaziente.Boundary;


import java.awt.EventQueue;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;


import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import ClientPaziente.Proxy.ClientProxy;

public class AccessoUI extends JFrame {

	private JPanel contentPane;
	private JTextField cfField;
	private JTextField passwordField;
	private JLabel cfLabel;
	private JLabel passLabel;
	private JLabel msg;
	

	/**
	 * Launch the application.
	 */
	public static void accedi() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AccessoUI frame = new AccessoUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public AccessoUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnNewButton = new JButton("accedi");
		btnNewButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				//Verifica se i dati di accesso sono validi:
				String cf = cfField.getText();
				String pw = passwordField.getText();
				
				cf = cf.toUpperCase();
				if(verificaCF(cf) == false || pw.length() < 8) {
					System.out.println("Verifica preventiva fallita");
					msg.setVisible(true);
					cfLabel.setBounds(56, 60, 45, 13);
					contentPane.add(cfLabel);
					return;
				}
				
				ClientProxy p = new ClientProxy();
				
				if (p.login(cf, pw)) {
					Menu.CF=cfField.getText();
					cfField.setVisible(false);
					passwordField.setVisible(false);
					cfLabel.setVisible(false);
					passLabel.setVisible(false);
					
					msg.setText("Accesso effettuato");
					msg.setVisible(true);
					System.out.println("logged");
					Menu.login=true;
					Menu.frame.loggedIn(true);
				}
				else {
					msg.setVisible(true);
					cfLabel.setBounds(56, 60, 45, 13);
					contentPane.add(cfLabel);
				}
			}
		});
		btnNewButton.setBounds(149, 197, 85, 21);
		contentPane.add(btnNewButton);
		
		cfLabel = new JLabel("CF:");
		cfLabel.setBounds(43, 60, 58, 13);
		contentPane.add(cfLabel);
		
		passLabel = new JLabel("password:");
		passLabel.setBounds(43, 111, 85, 13);
		contentPane.add(passLabel);
		
		cfField = new JTextField();
		cfField.setBounds(149, 57, 172, 19);
		contentPane.add(cfField);
		cfField.setColumns(10);
		
		passwordField = new JTextField();
		passwordField.setBounds(149, 108, 172, 19);
		contentPane.add(passwordField);
		passwordField.setColumns(10);
		
		msg = new JLabel("Accesso negato");
		msg.setVisible(false);
		msg.setBounds(188, 156, 314, 13);
		
		contentPane.add(msg);
		
		JButton menuButton = new JButton("menu");
		menuButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				//Menu.start();
				CloseFrame();
			}
		});
		menuButton.setBounds(251, 197, 85, 21);
		contentPane.add(menuButton);
		
	}
	
	public void CloseFrame() {
		super.dispose();
	}
	
	
	private boolean verificaCF(String cf) {
		if( cf.length()!=16 ) {
			return false;
		}
		int[] alpPos = {0,1,2,3,4,5,8,11,15};
		int[] numPos = {6,7,9,10,12,13,14};
		for(int i: alpPos) {
			if( !Character.isLetter(cf.charAt(i))) {
				return false;
			}
		}
		for(int i: numPos) {
			if( !Character.isDigit(cf.charAt(i))) {
				return false;
			}
		}
		String mesiValidi = "ABCDEHLMPRST";
		Character mese = cf.charAt(8);
		if(mesiValidi.indexOf(mese) == -1) {
			return false;
		}
		
		String alpOdd = "BAKPLCQDREVOSFTGUHMINJWZYX";
		String numOdd = "10   2 3 4   5 6 7 8 9    ";
		int checksum = 0;
		for(int i = 1; i < 15; i+=2) {
			//even
			if(Character.isDigit(cf.charAt(i))) {
				checksum += Character.getNumericValue(cf.charAt(i));
			}else {
				checksum += (int)cf.charAt(i) - (int)'A';
			}
		}
		for(int i = 0; i<15; i+=2) {
			//odd
			if(Character.isDigit(cf.charAt(i))) {
				checksum += numOdd.indexOf(cf.charAt(i));
			}else {
				checksum += alpOdd.indexOf(cf.charAt(i));
			}
		}
		checksum = checksum % 26;
		Character check = (char) (checksum + (int)'A');
		if(cf.charAt(15) != check) {
			return false;
		}
		
		return true;
	}
}
