package ClientPaziente.Boundary;

import java.awt.EventQueue;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import ClientPaziente.Proxy.ClientProxy;

import javax.swing.JLabel;
import javax.swing.JTextPane;
import javax.swing.JButton;

public class CondizioniAttualiUI extends JFrame {

	private JPanel contentPane;
	private JLabel terapieLabel;
	private JTextPane textPane; 
	/**
	 * Launch the application.
	 */
	public static void mostra(String cfpaziente) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CondizioniAttualiUI frame = new CondizioniAttualiUI();
					frame.setVisible(true);
					ClientProxy p = new ClientProxy();
					String terapie = p.richiediPrescrizione(cfpaziente);
					frame.textPane.setText(terapie.toString());
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public CondizioniAttualiUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 440, 350);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		contentPane.setLayout(null);
		
		terapieLabel = new JLabel("Terapie in corso");
		terapieLabel.setBounds(20, 20, 150, 20);
		contentPane.add(terapieLabel);
		
		textPane = new JTextPane();
		textPane.setBounds(20, 52, 408, 200);
		textPane.setText("Caricamento...");
		contentPane.add(textPane);
		
		JButton btnChiudi = new JButton("Chiudi");
		btnChiudi.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				dispose();
			}
		});
		btnChiudi.setBounds(165, 270, 110, 25);
		contentPane.add(btnChiudi);
	}
}
