package ClientPaziente.Boundary;


import java.awt.EventQueue;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;



public class Menu extends JFrame {

	private JPanel contentPane;

	
	private JButton accessoButton;
	private JButton prenotaButton;
	private JButton terapieButton;
	private JButton sintomoButton;
	private JButton misurazioneButton;
	private JButton uscitaButton;

	public static String CF;
	public static boolean login=false;

	public static Menu frame;
	/**
	 * Launch the application.
	 */
	public static void start() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					frame = new Menu();
					frame.setVisible(true);
					frame.loggedIn(login);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Menu() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		prenotaButton = new JButton("richiedi visita");
		prenotaButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if(login==true) {
					PrenotazioneUI.prenota(CF);
				}
				else {
					AccessoUI.accedi();
				}
			}
		});
		
		prenotaButton.setBounds(143, 70, 204, 21);
		contentPane.add(prenotaButton);
		
		JButton prescrizioniButton = new JButton("visualizza prescrizioni mediche");
		prescrizioniButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (login==true) {
					CondizioniAttualiUI.mostra(CF);
				}
				else {
					AccessoUI.accedi();
				}
				
			}
		});
		prescrizioniButton.setBounds(143, 112, 204, 21);
		contentPane.add(prescrizioniButton);
		
		JButton sintomoButton = new JButton("inserisci nuovo sintomo");
		sintomoButton.setVisible(false);
		sintomoButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
			}
		});
		sintomoButton.setBounds(143, 201, 204, 21);
		contentPane.add(sintomoButton);
		
		JButton misurazioniButton = new JButton("inserisci nuova misurazione");
		misurazioniButton.setVisible(false);
		misurazioniButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				
			}
		});
		misurazioniButton.setBounds(143, 232, 204, 21);
		contentPane.add(misurazioniButton);
		
		accessoButton = new JButton("accedi");
		accessoButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				
				AccessoUI.accedi();
				//dispose();
			}
		});
		accessoButton.setBounds(143, 22, 204, 21);
		contentPane.add(accessoButton);
		
		uscitaButton = new JButton("esci");
		uscitaButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				login=false;
				CF= null;
				loggedIn(false);
				
			}
		});
		uscitaButton.setEnabled(false);
		uscitaButton.setBounds(143, 155, 204, 21);
		contentPane.add(uscitaButton);
	}
	
	public void loggedIn(boolean loggato) {
		accessoButton.setEnabled(!loggato);
		uscitaButton.setEnabled(loggato);
	}
}


