package ClientPaziente.Boundary;


import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import ClientPaziente.Proxy.ClientProxy;


import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JRadioButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;


import java.sql.Timestamp;
import java.util.List;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class PrenotazioneUI extends JFrame {

	private JPanel contentPane;
	
	private JLabel oneLabel;
	private JLabel twoLabel;
	private JLabel threeLabel;
	private JLabel fourLabel;
	private JLabel fiveLabel;
	private JLabel sixLabel;
	private JLabel sevenLabel;
	private JLabel eightLabel;
	private JLabel nineLabel;
	private JLabel tenLabel;
	
	private JRadioButton radio1Button;
	private JRadioButton radio2Button;
	private JRadioButton radio3Button;
	private JRadioButton radio4Button;
	private JRadioButton radio5Button;
	private JRadioButton radio6Button;
	private JRadioButton radio7Button;
	private JRadioButton radio8Button;
	private JRadioButton radio9Button;
	private JRadioButton radio10Button;
	
	private List<Timestamp> date;
	private Timestamp time_scelto;
	
	/**
	 * Launch the application.
	 */
	public static void prenota(String CF) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PrenotazioneUI frame = new PrenotazioneUI(CF);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	

	
	public PrenotazioneUI(String CF) {
		
		ClientProxy p = new ClientProxy();
		System.setProperty("java.rmi.server.hostname","localhost");
		date= p.richiediVisita(CF, false, 1, null);
		
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		createLabels();
		
		createRadioButton();
		
		JButton page1Button = new JButton("1");
		page1Button.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				
				date= p.richiediVisita(CF, false, 1, null);
				setLabels();
			}
		});
		page1Button.setBounds(101, 240, 45, 21);
		contentPane.add(page1Button);
		
		JButton page2Button = new JButton("2");
		page2Button.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				date= p.richiediVisita(CF, false, 2, null);
				setLabels();
			}
		});
		page2Button.setBounds(150, 240, 45, 21);
		contentPane.add(page2Button);
		
		JButton page3Button = new JButton("3");
		page3Button.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				date= p.richiediVisita(CF, false, 3, time_scelto);
				setLabels();
			}
		});
		page3Button.setBounds(199, 240, 45, 21);
		contentPane.add(page3Button);
		
		JButton page4Button = new JButton("4");
		page4Button.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				date= p.richiediVisita(CF, false, 4, time_scelto);
				setLabels();
			}
		});
		page4Button.setBounds(248, 240, 45, 21);
		contentPane.add(page4Button);
		
		JButton page5Button_1 = new JButton("5");
		page5Button_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				date= p.richiediVisita(CF, false, 5, time_scelto);
				setLabels();
				
				
			}
		});
		page5Button_1.setBounds(299, 240, 45, 21);
		contentPane.add(page5Button_1);
		
	
		JButton OKButton = new JButton("OK");
		OKButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (radio1Button.isSelected()) {
					time_scelto=date.get(0);
				}
				if (radio2Button.isSelected()) {
					time_scelto=date.get(1);
				}
				if (radio2Button.isSelected()) {
					time_scelto=date.get(2);
				}
				if (radio3Button.isSelected()) {
					time_scelto=date.get(3);
				}
				if (radio4Button.isSelected()) {
					time_scelto=date.get(4);
				}
				if (radio5Button.isSelected()) {
					time_scelto=date.get(5);
				}
				if (radio6Button.isSelected()) {
					time_scelto=date.get(6);
				}
				if (radio7Button.isSelected()) {
					time_scelto=date.get(7);
				}
				if (radio8Button.isSelected()) {
					time_scelto=date.get(8);
				}
				if (radio9Button.isSelected()) {
					time_scelto=date.get(9);
				}
				if (radio10Button.isSelected()) {
					time_scelto=date.get(10);
				}
				System.out.println(date.toString());
				p.richiediVisita(CF, true, 0,time_scelto );
			}
		});
		OKButton.setBounds(253, 38, 85, 86);
		contentPane.add(OKButton);
		
		JButton btnTorna = new JButton("Men\u00F9");
		btnTorna.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				Menu.start();
			}
		});
		btnTorna.setBounds(256, 130, 85, 86);
		contentPane.add(btnTorna);
	}
	
	private void  createLabels() {
		
		JLabel dateLabel = new JLabel("seleziona una data e premi \"Ok\"oppure seleziona una pagina");
		dateLabel.setBounds(52, 0, 354, 13);
		contentPane.add(dateLabel);
		
		JLabel pageLabel = new JLabel("pagina");
		pageLabel.setBounds(52, 244, 45, 13);
		contentPane.add(pageLabel);
		
		oneLabel = new JLabel(this.date.get(0).toString());
		oneLabel.setVisible(true);
		oneLabel.setBounds(82, 19, 164, 13);
		contentPane.add(oneLabel);
		
		twoLabel = new JLabel(this.date.get(1).toString());
		twoLabel.setVisible(true);
		twoLabel.setBounds(82, 42, 164, 13);
		contentPane.add(twoLabel);
		
		threeLabel = new JLabel(this.date.get(2).toString());
		threeLabel.setVisible(true);
		threeLabel.setBounds(82, 65, 164, 13);
		contentPane.add(threeLabel);
		
		fourLabel = new JLabel(this.date.get(3).toString());
		fourLabel.setVisible(true);
		fourLabel.setBounds(82, 88, 164, 13);
		contentPane.add(fourLabel);
		
		fiveLabel = new JLabel(this.date.get(4).toString());
		fiveLabel.setVisible(true);
		fiveLabel.setBounds(82, 111, 164, 13);
		contentPane.add(fiveLabel);
		
		sixLabel = new JLabel(this.date.get(5).toString());
		sixLabel.setVisible(true);
		sixLabel.setBounds(82, 134, 164, 13);
		contentPane.add(sixLabel);
		
		sevenLabel = new JLabel(this.date.get(6).toString());
		sevenLabel.setVisible(true);
		sevenLabel.setBounds(82, 154, 164, 13);
		contentPane.add(sevenLabel);
		
		eightLabel = new JLabel(this.date.get(7).toString());
		eightLabel.setVisible(true);
		eightLabel.setBounds(82, 177, 164, 13);
		contentPane.add(eightLabel);
		
		nineLabel = new JLabel(this.date.get(8).toString());
		nineLabel.setVisible(true);
		nineLabel.setBounds(82, 200, 164, 13);
		contentPane.add(nineLabel);
		
		tenLabel = new JLabel(this.date.get(9).toString());
		tenLabel.setVisible(true);
		tenLabel.setBounds(82, 221, 164, 13);
		contentPane.add(tenLabel);
	
	}
	
	private void setLabels() {
		oneLabel.setText(this.date.get(0).toString());
		twoLabel.setText(this.date.get(1).toString());
		threeLabel.setText(this.date.get(2).toString());
		fourLabel.setText(this.date.get(3).toString());
		fiveLabel.setText(this.date.get(4).toString());
		sixLabel.setText(this.date.get(5).toString());
		sevenLabel.setText(this.date.get(6).toString());
		eightLabel.setText(this.date.get(7).toString());
		nineLabel.setText(this.date.get(8).toString());
		tenLabel.setText(this.date.get(9).toString());
	
	}
	
	private void createRadioButton() {
		
		radio1Button = new JRadioButton("1");
		radio1Button.setBounds(37, 15, 39, 21);
		contentPane.add(radio1Button);
		
		radio2Button = new JRadioButton("2");
		radio2Button.setBounds(37, 38, 39, 21);
		contentPane.add(radio2Button);
		
		radio3Button = new JRadioButton("3");
		radio3Button.setBounds(37, 61, 39, 21);
		contentPane.add(radio3Button);
		
		radio4Button = new JRadioButton("4");
		radio4Button.setBounds(37, 84, 39, 21);
		contentPane.add(radio4Button);
		
		radio5Button = new JRadioButton("5");
		radio5Button.setBounds(37, 107, 39, 21);
		contentPane.add(radio5Button);
		
		radio6Button = new JRadioButton("6");
		radio6Button.setBounds(37, 130, 39, 21);
		contentPane.add(radio6Button);
		
		radio7Button = new JRadioButton("7");
		radio7Button.setBounds(37, 150, 39, 21);
		contentPane.add(radio7Button);
		
		radio8Button = new JRadioButton("8");
		radio8Button.setBounds(37, 173, 45, 21);
		contentPane.add(radio8Button);
		
		radio9Button = new JRadioButton("9");
		radio9Button.setBounds(37, 196, 39, 21);
		contentPane.add(radio9Button);
		
		radio10Button = new JRadioButton("10");
		radio10Button.setBounds(37, 217, 39, 21);
		contentPane.add(radio10Button);
	}
}
