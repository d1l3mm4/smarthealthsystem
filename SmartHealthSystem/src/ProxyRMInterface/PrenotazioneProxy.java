package ProxyRMInterface;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.sql.Timestamp;
import java.util.List;

public interface PrenotazioneProxy extends Remote {

	List<Timestamp> richiediVisita(String CF, boolean scelta, int pagina, Timestamp time) throws RemoteException;
}