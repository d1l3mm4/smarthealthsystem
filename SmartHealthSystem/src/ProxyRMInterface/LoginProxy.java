package ProxyRMInterface;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface LoginProxy extends Remote {

	boolean login(String CF, String password) throws RemoteException;
	
}