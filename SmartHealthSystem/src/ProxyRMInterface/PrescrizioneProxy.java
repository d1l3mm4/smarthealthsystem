package ProxyRMInterface;

import java.rmi.Remote;
import java.rmi.RemoteException;


public interface PrescrizioneProxy extends Remote {

	String richiediPrescrizione(String CF) throws RemoteException;
}