package Server.Domain;

import java.sql.Timestamp;

import Server.DBWrapper.VisitaDB;

//TODO: TOGLIERE CONFERMATA
public class Visita {

	private Timestamp data;
	private boolean domiciliare = false;
	// private boolean confermata = false;
	private int id;
	private Paziente p;

	public Visita() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Visita(String CF, Timestamp data, boolean domiciliare, boolean confermata) {
		super();
		this.data = data;
		this.domiciliare = domiciliare;
		// this.confermata = confermata;
		Paziente paz= new Paziente(CF);
		this.p=paz;
		this.salva();
	}

	public Visita(VisitaDB v) {
		super();
		this.data = v.getData();
		this.domiciliare = v.isDomiciliare();
		// this.confermata= v.isConfermata();
		this.id = v.getId();
		this.p = new Paziente(v.getP());
	}
/*
	public static ArrayList<Visita> getVisite() {
		ArrayList<Visita> v = new ArrayList<Visita>();
		ArrayList<VisitaDB> vDB = VisitaDB.getVisite();

		for (VisitaDB vdb : vDB) {
			Visita temp = new Visita(vdb);
			v.add(temp);
		}

		return v;

	}
*/
	public String toString() {

		StringBuilder ret = new StringBuilder();
		ret.append("Visita con id ");
		ret.append(this.id);
		ret.append(" in data ");
		ret.append(this.data.toString());
		ret.append(" per il paziente ");
		ret.append(this.p.getNome() + " " + this.p.getCognome());

		if (this.domiciliare == true) {
			ret.append(" -visita domiciliare ;)");
		}
		return ret.toString();
	}
	
	public void salva() {
		VisitaDB v= new VisitaDB();
		v.setData(data);
		v.setDomiciliare(domiciliare);
		v.setP(this.p.getPazienteDB());
		
		v.salva();
		
		System.out.println("ID visita creata: " + v.getId());
		this.id = v.getId();
		

	}

}