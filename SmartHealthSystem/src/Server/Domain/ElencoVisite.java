package Server.Domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import Server.DBWrapper.VisitaDB;

public class ElencoVisite {
	
	private ArrayList<Visita> prenotazioni;
	
	
	public ElencoVisite() {
		
		ArrayList<Visita> visite = new ArrayList<Visita>();
		
		ArrayList<VisitaDB> prenotazioniDB = new ArrayList<VisitaDB>();
		prenotazioniDB = VisitaDB.getVisite();
		
		if(prenotazioniDB != null) {
			
			for(VisitaDB vdb : prenotazioniDB) {
				
				Visita v = new Visita(vdb);
	
				visite.add(v);
			}
		}
		
		this.prenotazioni=visite;
	
	}
	
	public ArrayList<Visita> getPrenotazioni() {
		return prenotazioni;
	}
	
	public static List<Timestamp> getProssimeDate(int pagina){
		return VisitaDB.getProssimeDate(pagina);
	}
	
	@Override
	public String toString() {
		StringBuilder ret= new StringBuilder();
		for (Visita v : this.prenotazioni) {
			ret.append(v.toString());
			ret.append("\n");
		}
		return ret.toString();
	}
	
	public static void inserisciVisita(String CF, Timestamp time ) {
		Visita v= new Visita(CF, time, false,false );
	}
	
}
