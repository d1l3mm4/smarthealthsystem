package Server.Domain;

import java.util.*;

import Server.DBWrapper.PazienteDB;
import Server.DBWrapper.TerapiaDB;
import Server.Service.TwilioUtil;

import java.sql.Date;

public class Paziente {

	private ArrayList<Terapia> storico;

	private String nome;
	private String cognome;
	private String CF;
	private String gruppoSanguigno;
	private String allergia;
	private String dipendenze;
	private Date dataNascita;
	private String recapitoTelefonico;
	private Date dataMorte;
	private String causaMorte;
	private String password;
	private char sesso;
	private String residenza;

	public Paziente(String nome, String cognome, String cF, String gruppoSanguigno, String allergia, String dipendenze,
			Date dataNascita, String recapitoTelefonico, char sesso, String residenza) {
		super();
		this.nome = nome;
		this.cognome = cognome;
		this.CF = cF;
		this.gruppoSanguigno = gruppoSanguigno;
		this.allergia = allergia;
		this.dipendenze = dipendenze;
		this.dataNascita = dataNascita;
		this.recapitoTelefonico = recapitoTelefonico;
		this.sesso = sesso;
		this.password = Paziente.generatePassword();
		this.residenza = residenza;
		
		try {
			this.salva();
			TwilioUtil.smsSender(this.recapitoTelefonico, this.password);
		}catch(Exception e) {
			this.CF = null;
			e.printStackTrace();
		}
	}

	public Paziente() {
		super();
	}

	public PazienteDB getPazienteDB() {

		PazienteDB pDB = new PazienteDB();
		pDB = pDB.getPazienteByPK(this.CF);

		return pDB;
	}

	public Paziente(String codicefiscale) {

		super();

		PazienteDB self = null;
		try {
			self = new PazienteDB(codicefiscale);
		}catch(Exception e) {
			System.out.println(e.toString());
			self = null;
			this.CF = null;
		}
		if(self != null) {
	
			this.nome = self.getNome();
			this.cognome = self.getCognome();
			this.CF = self.getCF();
			this.gruppoSanguigno = self.getGruppoSanguigno();
			this.allergia = self.getAllergia();
			this.dipendenze = self.getDipendenze();
			this.dataNascita = self.getDataNascita();
			this.dataMorte = self.getDataMorte();
			this.recapitoTelefonico = self.getRecapitoTelefonico();
			this.residenza = self.getResidenza();
			this.sesso = self.getSesso();
			this.password = self.getPassword();
	
			this.storico = new ArrayList<Terapia>();
			if(self.getStorico() != null) {
				for (TerapiaDB t : self.getStorico()) {
					this.storico.add(new Terapia(t));
				}
			}
		}
	}

	public Paziente(PazienteDB self) {
		super();

		this.nome = self.getNome();
		this.cognome = self.getCognome();
		this.CF = self.getCF();
		this.gruppoSanguigno = self.getGruppoSanguigno();
		this.allergia = self.getAllergia();
		this.dipendenze = self.getDipendenze();
		this.dataNascita = self.getDataNascita();
		this.dataMorte = self.getDataMorte();
		this.recapitoTelefonico = self.getRecapitoTelefonico();
		this.residenza = self.getResidenza();
		this.sesso = self.getSesso();
		this.password = self.getPassword();

		this.storico = new ArrayList<Terapia>();
		for (TerapiaDB t : self.getStorico()) {
			this.storico.add(new Terapia(t));
		}
	}

	public Paziente salva() {

		PazienteDB p = new PazienteDB();

		p.setCF(this.CF);
		p.setNome(nome);
		p.setCognome(cognome);
		p.setGruppoSanguigno(gruppoSanguigno);
		p.setAllergia(allergia);
		p.setDipendenze(dipendenze);
		p.setDataNascita(dataNascita);
		p.setRecapitoTelefonico(recapitoTelefonico);
		p.setSesso(sesso);
		p.setPassword(password);
		p.setResidenza(residenza);

		p.salva();
		return this;

	}


	private static String generatePassword() {

		String ALPHA_NUMERIC_STRING = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!�$%&#@?";
		int count = 12;

		StringBuilder builder = new StringBuilder();
		while (count-- != 0) {
			int character = (int) (Math.random() * ALPHA_NUMERIC_STRING.length());
			builder.append(ALPHA_NUMERIC_STRING.charAt(character));
		}
		// System.out.println(builder.toString());
		return builder.toString();

	}

	public ArrayList<Terapia> getStorico() {
		return storico;
	}
	
	public String getstorico() {
	StringBuilder ret = new StringBuilder();
	if (this.getStorico()==null || this.getStorico().size()==0) {
		return ("non ci sono terapie effettuate per il paziente " + this.getNome() + " " + this.getCognome() );
	}
	for (Terapia t : this.getStorico()) {
		ret.append(t.toString());
		ret.append("\n");
	}
		return ret.toString();
	}

	public String visualizzaCondizioniAttuali() {
		StringBuilder ret= new StringBuilder();
		ArrayList<Terapia> terapieAttuali = this.getStorico();
		if (terapieAttuali ==null || terapieAttuali.size()==0) {
			return ("non ci sono terapie in corso per il paziente " + this.getNome() + " " + this.getCognome() );
		}
		for (Terapia t : terapieAttuali) {
			if (t.inCorso()) {
				ret.append(t.toString());
				ret.append("\n");
			}
		}
		
		return ret.toString();
	}
	
	public ArrayList<Terapia> visualizzacondizioniAttuali() {

		ArrayList<Terapia> terapieAttuali = new ArrayList<Terapia>();
		for (Terapia t : this.getStorico()) {
			if (t.inCorso()) {
				terapieAttuali.add(t);
			}
		}
		
		return terapieAttuali;
	}

	public void setStorico(ArrayList<Terapia> storico) {
		this.storico = storico;
	}


	public String toString() {
		StringBuilder ret = new StringBuilder();

		ret.append(" nome: " + nome);
		ret.append(" cognome: " + cognome);
		ret.append(" \n");
		ret.append(" sesso " + sesso);
		ret.append(" CF: " + CF);
		ret.append(" data di nascita: " + dataNascita);
		ret.append(" \n");
		ret.append(" residenza: " + residenza);
		ret.append(" \n");
		ret.append(" allergie: " + allergia);
		ret.append(" \n");
		ret.append(" dipendenze: " + dipendenze);
		ret.append(" \n");
		ret.append(" gruppo sanguingno: " + gruppoSanguigno);
		ret.append(" recapito telefonico: " + recapitoTelefonico);

		if (dataMorte != null) {
			ret.append("\n data morte: " + dataMorte);
			ret.append(" causa morte " + causaMorte);
		}
		/*
		 * for(Terapia s : this.storico) { s.print(); }
		 */
		return ret.toString();
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCognome() {
		return cognome;
	}

	public void setCognome(String cognome) {
		this.cognome = cognome;
	}

	public String getCF() {
		return CF;
	}

	public void setCF(String cF) {
		CF = cF;
	}

	public String getGruppoSanguigno() {
		return gruppoSanguigno;
	}

	public void setGruppoSanguigno(String gruppoSanguigno) {
		this.gruppoSanguigno = gruppoSanguigno;
	}

	public String getAllergia() {
		return allergia;
	}

	public void setAllergia(String allergia) {
		this.allergia = allergia;
	}

	public String getDipendenze() {
		return dipendenze;
	}

	public void setDipendenze(String dipendenze) {
		this.dipendenze = dipendenze;
	}

	public Date getDataNascita() {
		return dataNascita;
	}

	public void setDataNascita(Date dataNascita) {
		this.dataNascita = dataNascita;
	}

	public String getRecapitoTelefonico() {
		return recapitoTelefonico;
	}

	public void setRecapitoTelefonico(String recapitoTelefonico) {
		this.recapitoTelefonico = recapitoTelefonico;
	}

	public Date getDataMorte() {
		return dataMorte;
	}

	public void setDataMorte(Date dataMorte) {
		this.dataMorte = dataMorte;
	}

	public String getCausaMorte() {
		return causaMorte;
	}

	public void setCausaMorte(String causaMorte) {
		this.causaMorte = causaMorte;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public char getSesso() {
		return sesso;
	}

	public void setSesso(char sesso) {
		this.sesso = sesso;
	}

	public String getResidenza() {
		return residenza;
	}

	public void setResidenza(String residenza) {
		this.residenza = residenza;
	}

}