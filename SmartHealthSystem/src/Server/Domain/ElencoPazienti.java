package Server.Domain;
import java.sql.Date;
import java.util.ArrayList;

import Server.DBWrapper.PazienteDB;

public class ElencoPazienti {
	
	private ArrayList<Paziente> pazienti;
	
	public ArrayList<Paziente> getPazienti() {
		return pazienti;
	}

	public ElencoPazienti() {
		super();
	}
	
	public ElencoPazienti( ArrayList<Paziente> p) {
		super();
		this.pazienti=p;
	}
		
	public ElencoPazienti(String nome, String cognome) {

		ArrayList<PazienteDB> pDB_list = new ArrayList<PazienteDB>();
		ArrayList<Paziente> p_list = new ArrayList<Paziente>();
		pDB_list = PazienteDB.findPazientiByNome(nome, cognome);

		for (PazienteDB pdb : pDB_list) {
			Paziente p = new Paziente(pdb);
			p_list.add(p);
		}
		pazienti=p_list;
		
	}
	
	public static boolean login(String CF, String password) {
		
		Paziente p = new Paziente(CF);
		
		if (p.getCF()==null) {
			return false;
		}
		
		String match= p.getPassword();
		if( match.equals(password)) {
			return true;
		}
		else 
			return false;
	}

//	public static ArrayList<Paziente> getPazientiInCura() {
//		ArrayList<Paziente> p_list = new ArrayList<Paziente>();
//		ArrayList<PazienteDB> pDB_list = new ArrayList<PazienteDB>();
//
//		pDB_list = PazienteDB.getPazientiInCura();
//
//		for (PazienteDB pdb : pDB_list) {
//			Paziente p = new Paziente(pdb);
//			p_list.add(p);
//		}
//
//		return p_list;
//
//	}
	
	public static String getPazientiInCura() {
		StringBuilder ret= new StringBuilder();
		
		ArrayList<Paziente> p_list = new ArrayList<Paziente>();
		ArrayList<PazienteDB> pDB_list = new ArrayList<PazienteDB>();

		pDB_list = PazienteDB.getPazientiInCura();
		if(pDB_list != null) {
			for (PazienteDB pdb : pDB_list) {
				Paziente p = new Paziente(pdb);
				p_list.add(p);
				ret.append(p.toString());
				ret.append("\n");
			}
		}
		return ret.toString();

	}
	
	public static boolean terminaTerapia(String CF, int id) {
		Paziente p = new Paziente(CF);
		if (p.getCF()==null) {
			return false;
		}
		
		ArrayList<Terapia> t = new ArrayList<Terapia>();
		t = p.visualizzacondizioniAttuali();
		
		boolean terapiatrovata=false;
		
		for (int i = 0; i < t.size(); i++) {
			if (t.get(i).getId() == id) {
				t.get(i).termina();
				terapiatrovata=true;
			}
		}
		
		return terapiatrovata;

	}
	
	public static boolean inserisciPaziente(String nome, String cognome, String CF, String gruppoSanguigno, String allergia,
			String dipendenze, Date dataNascita, String recapitoTelefonico, char sesso, String residenza) {

		Paziente P = new Paziente(nome, cognome, CF, gruppoSanguigno, allergia, dipendenze, dataNascita,
				recapitoTelefonico, sesso, residenza);
		return (P.getCF() != null);
		// throw new UnsupportedOperationException();

	}
	
	public static String visualizzaCondizioniAttuali(String CF) {
		Paziente p = new Paziente(CF);
		return p.visualizzaCondizioniAttuali();
	}
	
	@Override
	public String toString() {
		StringBuilder ret = new StringBuilder();
		
		for (Paziente p : pazienti) {
			ret.append(p.toString());
			ret.append("\n");
		}
		
		return ret.toString();
	}
	
	public static String visualizzaStorico(String CF) {
		Paziente p = new Paziente(CF);
		return p.getstorico();
	}

	public static boolean iniziaTerapia(String CF, String diagnosi) {

		Paziente p = new Paziente(CF);
		Terapia T = new Terapia(p, diagnosi);
		
		return (T.getId() != -1);
		
	}

	public static boolean iniziaTerapia(String CF, String diagnosi, String presc) {

		Paziente p = new Paziente(CF);
		Terapia T = new Terapia(p, diagnosi, presc);
		
		return (T.getId() != -1);
		
	}
	

}
