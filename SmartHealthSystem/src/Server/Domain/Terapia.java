package Server.Domain;

import java.sql.Date;
import java.time.LocalDate;

import Server.DBWrapper.TerapiaDB;

public class Terapia {

	private Date dataInizio;
	private Date dataFine;
	private String diagnosi;
	private String prescrizioni;
	private int id;

	public Terapia() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Terapia(Paziente p, String diagnosi) {
		super();

		LocalDate now_date = LocalDate.now();

		this.dataInizio = Date.valueOf(now_date);
		this.dataFine = null;
		this.diagnosi = diagnosi;
		this.prescrizioni = null;
		
		this.salva(p);
		
	}

	public Terapia(Paziente p, String diagnosi, String prescrizioni) {
		super();

		LocalDate now_date = LocalDate.now();

		this.dataInizio = Date.valueOf(now_date);
		this.dataFine = null;
		this.diagnosi = diagnosi;
		this.prescrizioni = prescrizioni;
		
		this.salva(p);
		
	}

	public Terapia(Date dataInizio, Date dataFine, String diagnosi, String prescrizioni) {
		super();
		this.dataInizio = dataInizio;
		this.dataFine = dataFine;
		this.diagnosi = diagnosi;
		this.prescrizioni = prescrizioni;
	}

	public Terapia(TerapiaDB myself) {
		super();
		this.dataInizio = myself.getDataInizio();
		this.dataFine = myself.getDataFine();
		this.diagnosi = myself.getDiagnosi();
		this.id = myself.getID();
		this.prescrizioni = myself.getPrescrizioni();
	}

	public Date getDataInizio() {
		return dataInizio;
	}

	public void setDataInizio(Date dataInizio) {
		this.dataInizio = dataInizio;
	}

	public Date getDataFine() {
		return dataFine;
	}

	public void setDataFine(Date dataFine) {
		this.dataFine = dataFine;
	}

	public String getDiagnosi() {
		return diagnosi;
	}

	public void setDiagnosi(String diagnosi) {
		this.diagnosi = diagnosi;
	}

	public String getPrescrizioni() {
		return prescrizioni;
	}

	public void setPrescrizioni(String prescrizioni) {
		this.prescrizioni = prescrizioni;
	}

	public boolean inCorso() {
		return (this.dataFine == null);
	}

	public void print() {
		System.out.println("Terapia per " + this.diagnosi);
		System.out.println("Iniziata il " + this.dataInizio.toString());
		if (this.inCorso()) {
			System.out.println("Terapia ancora in corso");
		} else {
			System.out.println("Terminata il " + this.dataFine.toString());
		}
		if(this.prescrizioni != null && this.prescrizioni.length()>1) {
			System.out.println("Prescrizioni relative: "+this.prescrizioni.toString());
		}
	}

	public String toString() {
		StringBuilder ret = new StringBuilder();
		ret.append("Terapia con id ");
		ret.append(this.id);
		ret.append("Terapia per ");
		ret.append(this.diagnosi);
		ret.append(" iniziata il ");
		ret.append(this.dataInizio.toString());
		if (!this.inCorso()) {
			ret.append(" e terminata il ");
			ret.append(this.dataFine.toString());
		} else {
			ret.append(" ed ancora in corso");
		}
		if(this.prescrizioni!=null && this.prescrizioni.length()>1) {
			ret.append(" alla quale sono associate le prescrizioni: ");
			ret.append(this.prescrizioni);
		}else {
			ret.append(" alla quale non sono associate prescrizioni.");
		}

		return ret.toString();
	}

	public void salva(Paziente p) {
		this.id = -1;

		TerapiaDB t = new TerapiaDB();

		t.setDiagnosi(diagnosi);
		t.setDataInizio(dataInizio);
		t.setDataFine(dataFine);
		t.setPaziente(p.getPazienteDB());
		t.setPrescrizioni(prescrizioni);
		
		t.salva();
		
		System.out.println("ID terapia creata: " + t.getID());
		this.id = t.getID();

	}

	public void termina() {

		TerapiaDB terapia = new TerapiaDB();
		terapia = terapia.getTerapiaByPK(this.id);

		LocalDate date1 = LocalDate.now();
		Date date = Date.valueOf(date1);

		terapia.setDataFine(date);

		terapia.update();

	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

}