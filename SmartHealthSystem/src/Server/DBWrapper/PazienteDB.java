package Server.DBWrapper;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Set;

import javax.persistence.*;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import Server.Service.HibernateUtil;

@Entity
@Table(name = "Pazienti")
public class PazienteDB {

	// Terapia storico;
	@Column(name = "nome", nullable = false)
	private String nome;
	@Column(name = "cognome")
	private String cognome;
	@Id
	private String CF;
	@Column(name = "gruppoSanguigno")
	private String gruppoSanguigno;
	@Column(name = "allergie")
	private String allergia;
	@Column
	private String dipendenze;
	@Column
	private Date dataNascita;
	@Column
	private String recapitoTelefonico;
	@Column
	private Date dataMorte;
	@Column
	private String causaMorte;
	@Column
	private String password;
	@Column
	private char sesso;
	@Column
	private String residenza;

	@OneToMany(fetch = FetchType.EAGER)
	@JoinColumn(name = "PazienteCF")
	private Set<TerapiaDB> storico;

	@OneToMany(fetch = FetchType.EAGER)
	@JoinColumn(name = "PazienteCF")
	private Set<VisitaDB> visite;

	public Set<TerapiaDB> getStorico() {
		return storico;
	}

	public void setStorico(Set<TerapiaDB> storico) {
		this.storico = storico;
	}

	public void setDataMorte(Date dataMorte) {
		this.dataMorte = dataMorte;
	}

	public void setCausaMorte(String causaMorte) {
		this.causaMorte = causaMorte;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setCognome(String cognome) {
		this.cognome = cognome;
	}

	public void setCF(String cF) {
		CF = cF;
	}

	public void setGruppoSanguigno(String gruppoSanguigno) {
		this.gruppoSanguigno = gruppoSanguigno;
	}

	public void setAllergia(String allergia) {
		this.allergia = allergia;
	}

	public void setDipendenze(String dipendenze) {
		this.dipendenze = dipendenze;
	}

	public void setDataNascita(Date dataNascita) {
		this.dataNascita = dataNascita;
	}

	public void setRecapitoTelefonico(String recapitoTelefonico) {
		this.recapitoTelefonico = recapitoTelefonico;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setSesso(char sesso) {
		this.sesso = sesso;
	}

	public void setResidenza(String residenza) {
		this.residenza = residenza;
	}

	public String getNome() {
		return nome;
	}

	public String getCognome() {
		return cognome;
	}

	public String getCF() {
		return CF;
	}

	public String getGruppoSanguigno() {
		return gruppoSanguigno;
	}

	public String getAllergia() {
		return allergia;
	}

	public String getDipendenze() {
		return dipendenze;
	}

	public Date getDataNascita() {
		return dataNascita;
	}

	public String getRecapitoTelefonico() {
		return recapitoTelefonico;
	}

	public Date getDataMorte() {
		return dataMorte;
	}

	public String getCausaMorte() {
		return causaMorte;
	}

	public String getPassword() {
		return password;
	}

	public char getSesso() {
		return sesso;
	}

	public String getResidenza() {
		return residenza;
	}

	public PazienteDB() {
		super();

	}

	// TODO: Correggere le exception
	public PazienteDB(String CF) throws Exception {
		super();
		PazienteDB pdb = getPazienteByPK(CF);
		if (pdb != null) {
			this.CF = CF;
			this.nome = pdb.nome;
			this.cognome = pdb.cognome;
			this.gruppoSanguigno = pdb.gruppoSanguigno;
			this.allergia = pdb.allergia;
			this.dipendenze = pdb.dipendenze;
			this.dataNascita = pdb.dataNascita;
			this.recapitoTelefonico = pdb.recapitoTelefonico;
			this.dataMorte = pdb.dataMorte;
			this.causaMorte = pdb.causaMorte;
			this.password = pdb.password;
			this.sesso = pdb.sesso;
			this.residenza = pdb.residenza;
		}else {
			throw new Exception("Codice fiscale non valido");
		}
	}

	public PazienteDB getPazienteByPK(String CF) {

		PazienteDB p = new PazienteDB();

		// apro la sessione e la transazione
		SessionFactory sf = HibernateUtil.getSessionFactory();
		Session session = sf.openSession();
		try {
			session.beginTransaction();
	
			// salvo il cliente
			p = (PazienteDB) session.get(PazienteDB.class, CF);
	
			// chiudo la transazione e la sessione
			session.getTransaction().commit();
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			session.close();
		}
		if (p == null) {
			System.out.println("paziente non trovato");
		}
		return p;

	}

	public static ArrayList<PazienteDB> findPazientiByNome(String nome, String cognome) {

		ArrayList<PazienteDB> result = null;
		
		// apro la sessione e la transazione
		SessionFactory sf = HibernateUtil.getSessionFactory();
		Session session = sf.openSession();
		try {
			session.beginTransaction();
	
			Query query = session.createQuery("from PazienteDB as p where p.nome= :nome and p.cognome= :cognome");
			query.setParameter("nome", nome);
			query.setParameter("cognome", cognome);
			result = (ArrayList<PazienteDB>) query.list();
	
			// chiudo la transazione e la sessione
			session.getTransaction().commit();
		}catch(Exception e) {
			e.printStackTrace();
		}finally{
			session.close();
		}
		if (result == null || result.size() == 0) {
			System.out.println("paziente non trovato");
		}

		return result;

	}

	public String getPassword(String CF) {
		String password = null;

		SessionFactory sf = HibernateUtil.getSessionFactory();
		Session session = sf.openSession();
		try {
			session.beginTransaction();
			
			Query query = session
					.createQuery("select p.password from PazienteDB as p.password where p.CF:=CF");
			query.setParameter("CF", CF);
			
			password =  query.list().toString();
		}catch(Exception e) {
			e.printStackTrace();
		}finally{
			session.close();
		}
		return password;
	}

	public static ArrayList<PazienteDB> getPazientiInCura() {
		
		ArrayList<PazienteDB> result = null;
		
		// apro la sessione e la transazione
		SessionFactory sf = HibernateUtil.getSessionFactory();
		Session session = sf.openSession();
		
		
		try {
			session.beginTransaction();
			Query query = session
				.createQuery("select distinct p from PazienteDB as p join p.storico as s with s.dataFine is null");
			result = (ArrayList<PazienteDB>) query.list();

			// chiudo la transazione e la sessione
			session.getTransaction().commit();
		}catch(Exception e) {
			e.printStackTrace();
		}finally{
			session.close();
		}

		if (result == null || result.size() == 0) {
			System.out.println("non ci sono pazienti in cura");
		}

		return result;

	}

	public PazienteDB salva() {
		// apro la sessione e la transazione
		SessionFactory sf = HibernateUtil.getSessionFactory();
		Session session = sf.openSession();
		
		try {
			session.beginTransaction();
			// salvo il cliente
			session.save(this);	
			// chiudo la transazione e la sessione
			session.getTransaction().commit();
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			session.close();
		}
		return this;
	}

}