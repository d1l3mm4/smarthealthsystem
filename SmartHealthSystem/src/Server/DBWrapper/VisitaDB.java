package Server.DBWrapper;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.persistence.*;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import Server.Service.HibernateUtil;

@Entity
@Table(name = "Visite")
public class VisitaDB {

	@Column(name = "data")
	private Timestamp data;

	@Column(name = "domiciliare")
	private boolean domiciliare = false;
	/*
	 * @Column(name="conferma") private boolean confermata = false;
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@ManyToOne
	@JoinColumn(name = "PazienteCF")
	private PazienteDB p;

	public void setP(PazienteDB p) {
		this.p = p;
	}

	public PazienteDB getP() {
		return p;
	}

	public VisitaDB() {
		super();
	}

	public Timestamp getData() {
		return data;
	}

	public void setData(Timestamp data) {
		this.data = data;
	}

	public boolean isDomiciliare() {
		return domiciliare;
	}

	public void setDomiciliare(boolean domiciliare) {
		this.domiciliare = domiciliare;
	}

	/*
	 * public boolean isConfermata() { return confermata; } public void
	 * setConfermata(boolean confermata) { this.confermata = confermata; }
	 */
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public static ArrayList<VisitaDB> getVisite() {
		
		ArrayList<VisitaDB> v = null;

		// apro la sessione e la transazione
		SessionFactory sf = HibernateUtil.getSessionFactory();
		Session session = sf.openSession();
		
		try {
			session.beginTransaction();
	
			Query query = session.createQuery("from VisitaDB as v where v.data > current_date()");// and
																									// v.confermata=true");
	
			v = (ArrayList<VisitaDB>) query.list();
	
			// chiudo la transazione e la sessione
			session.getTransaction().commit();
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			session.close();
		}
		if (v == null || v.size() == 0) {
			System.out.println("non vi � alcuna prenotazione");
		}

		return v;

	}

	public VisitaDB getVisitaByPK(int ID) {

		VisitaDB t = null;

		// apro la sessione e la transazione
		SessionFactory sf = HibernateUtil.getSessionFactory();
		Session session = sf.openSession();
		try {
			session.beginTransaction();
	
			// prendo la terapia
			t = (VisitaDB) session.get(VisitaDB.class, ID);
	
			// chiudo la transazione e la sessione
			session.getTransaction().commit();
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			session.close();
		}
		if (t == null) {
			System.out.println("terapia non trovata");
		}
		return t;

	}

	public VisitaDB salva() {
		// apro la sessione e la transazione
		SessionFactory sf = HibernateUtil.getSessionFactory();
		Session session = sf.openSession();
		try {
			session.beginTransaction();
	
			// salva visita
			session.save(this);
	
			// chiudo la transazione e la sessione
			session.getTransaction().commit();
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			session.close();
		}
		return this;
	}
	
	
	public static List<Timestamp> getProssimeDate(int page) {
		
		List results = null;
		ArrayList<Timestamp> dates = new ArrayList<Timestamp>();
		
		SessionFactory sf = HibernateUtil.getSessionFactory();
		Session session = sf.openSession();
		
		try {
			session.beginTransaction();
			String sql = "SELECT * FROM time_intervals WHERE interval_start > now() and interval_start NOT IN (SELECT data FROM Visite where data > now()) LIMIT ";
			if(page <= 0) {
				sql += "10";
			}else {
				sql += page*10+", "+(page+1)*10; 
			}
			SQLQuery query = session.createSQLQuery(sql);
			query.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
			results = query.list();
			
			session.getTransaction().commit();
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			session.close();
		}
		if(results != null) {
			for(Object d: results) {
				HashMap<String,Timestamp> obj = (HashMap<String, Timestamp>) d;
				dates.add((Timestamp) obj.values().toArray()[0]);
			}
			
		}
		return dates;
	}
}
