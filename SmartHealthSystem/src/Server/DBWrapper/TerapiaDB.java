package Server.DBWrapper;

import java.sql.Date;
import java.util.ArrayList;

import javax.persistence.*;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import Server.Service.HibernateUtil;

@Entity
@Table(name = "Terapie")
public class TerapiaDB {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int ID;
	@Column(name = "dataInizio")
	private Date dataInizio;
	@Column(name = "dataFine")
	private Date dataFine;
	@Column(name = "diagnosi")
	private String diagnosi;
	@Column(name = "prescrizioni")
	private String prescrizioni;

	@ManyToOne
	@JoinColumn(name = "PazienteCF")
	private PazienteDB paziente;

	public TerapiaDB() {
		super();

	}

	public TerapiaDB getTerapiaByPK(int ID) {

		TerapiaDB t = new TerapiaDB();

		// apro la sessione e la transazione
		SessionFactory sf = HibernateUtil.getSessionFactory();
		Session session = sf.openSession();
		try {
			session.beginTransaction();
	
			// prendo la terapia
			t = (TerapiaDB) session.get(TerapiaDB.class, ID);
	
			// chiudo la transazione e la sessione
			session.getTransaction().commit();
			session.close();
	
			if (t == null) {
				System.out.println("terapia non trovata");
			}
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			session.close();		
		}
		return t;

	}

	public Date getDataInizio() {
		return dataInizio;
	}

	public void setDataInizio(Date dataInizio) {
		this.dataInizio = dataInizio;
	}

	public Date getDataFine() {
		return dataFine;
	}

	public void setDataFine(Date dataFine) {
		this.dataFine = dataFine;
	}

	public String getDiagnosi() {
		return diagnosi;
	}

	public void setDiagnosi(String diagnosi) {
		this.diagnosi = diagnosi;
	}

	public String getPrescrizioni() {
		return prescrizioni;
	}

	public void setPrescrizioni(String prescrizioni) {
		this.prescrizioni = prescrizioni;
	}

	public int getID() {
		return ID;
	}

	public void setID(int ID) {
		this.ID = ID;
	}

	public PazienteDB getPaziente() {
		return paziente;
	}

	public void setPaziente(PazienteDB paziente) {
		this.paziente = paziente;
	}

	public ArrayList<TerapiaDB> findByCF(String CFpaziente) {
		
		ArrayList<TerapiaDB> result = null;
		// apro la sessione e la transazione
		SessionFactory sf = HibernateUtil.getSessionFactory();
		Session session = sf.openSession();
		try {
			session.beginTransaction();
	
			Query query = session.createQuery("from TerapiaDB as t where t.paziente= :paziente");
			query.setParameter("paziente", CFpaziente);
			result = (ArrayList<TerapiaDB>) query.list();
	
			// chiudo la transazione e la sessione
			session.getTransaction().commit();
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			session.close();
		}
		
		
		
		if (result == null || result.size() == 0) {
			System.out.println("Non vi sono terapie per il paziente");
		}
		
		return result;

	}

	public TerapiaDB salva() {
		// apro la sessione e la transazione
		SessionFactory sf = HibernateUtil.getSessionFactory();
		Session session = sf.openSession();
		
		try {
			session.beginTransaction();
	
			// salvo il cliente
			session.save(this);
	
			// chiudo la transazione e la sessione
			session.getTransaction().commit();
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			session.close();
		}
		return this;
	}

	public TerapiaDB update() {
		// apro la sessione e la transazione
		SessionFactory sf = HibernateUtil.getSessionFactory();
		Session session = sf.openSession();
		try {
			session.beginTransaction();
	
			// set terminazione terapia
			session.update(this);
	
			// chiudo la transazione e la sessione
			session.getTransaction().commit();
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			session.close();
		}
		return this;
	}
}