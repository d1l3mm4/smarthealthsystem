package Server;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;

import Server.Proxy.ProxyManager;

public class avvio {

	public static void main(String[] args) {
		
		try {
			
			//avvia il registry sul porto 1099 (default)
			LocateRegistry.createRegistry(1099);
			
			//inizializza i servizi offerti
			ProxyManager lServerProxyManager = new ProxyManager();
			lServerProxyManager.initProxy();
			
			System.out.println("Server ready!");
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
