package Server.Coordinator;

import java.sql.Timestamp;
import java.util.List;
import Server.Domain.ElencoVisite;
import Server.Domain.ElencoPazienti;

public class PazienteController {
	
	public static PazienteController istance = null;
	
	private PazienteController() {
		
	}
	
	 public static synchronized PazienteController getSingleton() {
	        if (istance == null) {
	            istance = new PazienteController();
	        }
	        return istance;
	    }
	 
	public List<Timestamp> richiediVisita(String CF, boolean scelta, int page, Timestamp time ) {
 
		if (scelta==true) {
		ElencoVisite.inserisciVisita(CF, time);
			return null;
		}
		else {
			return ElencoVisite.getProssimeDate(page);
		}
		
	}
	
	public boolean login(String CF, String password) {
	
		return ElencoPazienti.login(CF, password);
	}
	
	
	public String visualizzaPrescrizioniMediche(String CF) {
		
		return ElencoPazienti.visualizzaCondizioniAttuali(CF);
	}
}
