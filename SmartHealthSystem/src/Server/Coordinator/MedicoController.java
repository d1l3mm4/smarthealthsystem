package Server.Coordinator;

import java.sql.Date;
import java.time.LocalDateTime;

import Server.Domain.ElencoPazienti;
import Server.Domain.ElencoVisite;


public class MedicoController {
	
	public static MedicoController istance = null;
	
	private MedicoController() {
		
	}
	
	 public static synchronized MedicoController getSingleton() {
	        if (istance == null) {
	            istance = new MedicoController();
	        }
	        return istance;
	    }
	
	public boolean inserisciPaziente(String nome, String cognome, String CF, String gruppoSanguigno, String allergia,
		
		String dipendenze, Date dataNascita, String recapitoTelefonico, char sesso, String residenza) {
		return ElencoPazienti.inserisciPaziente(nome, cognome, CF, gruppoSanguigno, allergia, dipendenze, dataNascita, recapitoTelefonico, sesso, residenza);

	}

	public String ricercaPaziente(String nome, String cognome) {

		ElencoPazienti p_list= new ElencoPazienti(nome, cognome);
		System.out.println("Trovati " + p_list.getPazienti().size() + " pazienti con nome " + nome + ", " + cognome);
		return p_list.toString();
	}

	public String visualizzaCondizioniAttuali(String CF) {
		String stringa= ElencoPazienti.visualizzaCondizioniAttuali(CF);
		return stringa;
	}

	public String visualizzaStorico(String CF) {
		String storico=ElencoPazienti.visualizzaStorico(CF);
		
		return storico;
	}

	public boolean iniziaTerapia(String CF, String diagnosi) {
		return ElencoPazienti.iniziaTerapia(CF, diagnosi);
	}
	public boolean iniziaTerapia(String CF, String diagnosi, String presc) {
		return ElencoPazienti.iniziaTerapia(CF, diagnosi, presc);
	}

	public boolean terminaTerapia(String CF, int id) {
		
		return ElencoPazienti.terminaTerapia(CF, id);
		
	}
	
	public String visualizzaPazientiInCura() {
		return ElencoPazienti.getPazientiInCura();

	}
	
	public String visualizzaListaPrenotazioni() {
		ElencoVisite e= new ElencoVisite();
		return e.toString();

	}
	

	public boolean inserisciPrescrizione(int terapiaid, String nome, LocalDateTime dataInizio, LocalDateTime dataFine,
			int frequenza) {
		// TODO - implement StudioMedico.inserisciPrescrizione
		throw new UnsupportedOperationException();
	}


}
