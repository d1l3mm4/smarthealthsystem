package Server.Service;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

public class TwilioUtil {
	
	private static final String ACCOUNT_SID="AC55077c00f7819ad842253f99c8163495";
	private static final String AUTH_KEY="4a1a22f9c1875ec7278ebbd91236b027";
	private static final PhoneNumber sender = new PhoneNumber("+12033507423");
	
	public static String smsSender(String destinatario, String testo) {
		Twilio.init(ACCOUNT_SID, AUTH_KEY);
		Message m = Message.creator(new PhoneNumber(destinatario), sender, testo).create();
		System.out.println(m.getStatus());
		return m.getSid();
	}

}
