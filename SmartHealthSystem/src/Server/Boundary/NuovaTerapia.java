package Server.Boundary;


import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JTextArea;

import Server.Coordinator.MedicoController;



public class NuovaTerapia extends JFrame {

	private JPanel contentPane;
	private JTextField cfField;

	/**
	 * Launch the application.
	 */
	public static void inserisciNuovaTerapia() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					NuovaTerapia frame = new NuovaTerapia();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public NuovaTerapia() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 350);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel cfLabel = new JLabel("CF:");
		cfLabel.setBounds(25, 29, 45, 13);
		contentPane.add(cfLabel);
		
		cfField = new JTextField();
		cfField.setBounds(128, 26, 264, 19);
		contentPane.add(cfField);
		cfField.setColumns(10);
		
		JLabel diagnosiLabel = new JLabel("Diagnosi:");
		diagnosiLabel.setBounds(25, 75, 73, 13);
		contentPane.add(diagnosiLabel);
		
		JTextArea textArea = new JTextArea();
		textArea.setBounds(128, 74, 264, 62);
		contentPane.add(textArea);
		
		JTextArea prescrizioniTextArea = new JTextArea();
		prescrizioniTextArea.setBounds(128, 152, 264, 62);
		contentPane.add(prescrizioniTextArea);
		
		JLabel prescLabel = new JLabel("Prescrizioni;");
		prescLabel.setBounds(25, 153, 105, 13);
		contentPane.add(prescLabel);
		
		
		JButton saveButton = new JButton("Salva");
		saveButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				MedicoController m = MedicoController.getSingleton();
				String cf = cfField.getText();
				cf = cf.toUpperCase();
				String diagnosi = textArea.getText();
				String prescrizioni = prescrizioniTextArea.getText();
				boolean res = false;
				if(cf.length()==16) {
					try {
						res = m.iniziaTerapia(cf, diagnosi, prescrizioni);
					}catch(Exception ex) {
						ex.printStackTrace();
					}
				}
				if(!res) {
					JOptionPane.showMessageDialog(contentPane, "Impossibile inserire la terapia, riprova!", "Attenzione", JOptionPane.ERROR_MESSAGE);
				}else {
					JOptionPane.showMessageDialog(contentPane, "Terapia inserita correttamente", "Info", JOptionPane.INFORMATION_MESSAGE);
				}
			}
		});
		saveButton.setBounds(128, 268, 85, 21);
		contentPane.add(saveButton);
		
		JButton menuButton = new JButton("Menu");
		menuButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				Menu.start();
				dispose();
				
			}
		});
		menuButton.setBounds(307, 268, 85, 21);
		contentPane.add(menuButton);
		
	}
}
