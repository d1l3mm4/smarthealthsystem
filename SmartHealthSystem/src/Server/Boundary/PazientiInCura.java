package Server.Boundary;

import java.awt.EventQueue;
import java.awt.FlowLayout;
import javax.swing.JTextArea;
import javax.swing.JScrollPane;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JFrame;

import Server.Coordinator.MedicoController;

public class PazientiInCura extends JFrame {

	/**
	 * Launch the application.
	 */
	public static void getPazientiInCura() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PazientiInCura frame = new PazientiInCura();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public PazientiInCura() {
		
        JTextArea area= new JTextArea(15, 30);
        
        JScrollPane scrollPane = new JScrollPane(area,
            JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
            JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
        
        getContentPane().setLayout(new FlowLayout());
        
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);        
        
   
       
        getContentPane().add(scrollPane);
        setSize(400,370);
        setLocationRelativeTo(null);
        setVisible(true);
        
        MedicoController m = MedicoController.getSingleton();
        String pazienti = m.visualizzaPazientiInCura();
        if(pazienti.length()<=2) {
        	pazienti = "Non vi sono pazienti attualmente in cura";
        }
        area.setText(pazienti);
        
        JButton btnNewButton = new JButton("Menu");
        btnNewButton.addMouseListener(new MouseAdapter() {
        	@Override
        	public void mouseClicked(MouseEvent e) {
        		dispose();
        		Menu.start();
        	}
        });
        getContentPane().add(btnNewButton);
	}
}
