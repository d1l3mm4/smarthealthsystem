package Server.Boundary;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import Server.Coordinator.MedicoController;

import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class TerminaTerapia extends JFrame {

	private JPanel contentPane;
	private JTextField cfField;
	private JTextField idField;

	/**
	 * Launch the application.
	 */
	public static void terminaTerapia(
			) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TerminaTerapia frame = new TerminaTerapia();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public TerminaTerapia() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel cfLabel = new JLabel("CF:");
		cfLabel.setBounds(47, 56, 45, 13);
		contentPane.add(cfLabel);
		
		cfField = new JTextField();
		cfField.setBounds(140, 53, 139, 19);
		contentPane.add(cfField);
		cfField.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("id terapia:");
		lblNewLabel.setBounds(47, 108, 89, 13);
		contentPane.add(lblNewLabel);
		
		idField = new JTextField();
		idField.setBounds(140, 105, 139, 19);
		contentPane.add(idField);
		idField.setColumns(10);
		
		JLabel errorLabel = new JLabel("terapia non trovata oppure CF non valido");
		errorLabel.setBounds(101, 136, 267, 13);
		contentPane.add(errorLabel);
		errorLabel.setVisible(false);
		contentPane.add(errorLabel);
		
		JButton btntermina = new JButton("Termina");
		btntermina.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				MedicoController m= MedicoController.getSingleton();
				
				int id= Integer.parseInt(idField.getText());
				boolean errore = m.terminaTerapia(cfField.getText().toUpperCase(), id);
				if (!errore) {
					errorLabel.setVisible(true);
				}
				else {
					errorLabel.setText("terapia terminata con successo");
					errorLabel.setVisible(true);
				}
			}
		});
		btntermina.setBounds(175, 159, 85, 21);
		contentPane.add(btntermina);
		
		JButton btnMenu = new JButton("menu");
		btnMenu.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				Menu.start();
				dispose();
			}
		});
		btnMenu.setBounds(175, 194, 85, 21);
		contentPane.add(btnMenu);
	}
}
