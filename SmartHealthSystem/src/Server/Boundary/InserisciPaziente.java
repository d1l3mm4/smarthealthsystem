package Server.Boundary;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Date;
import java.text.SimpleDateFormat;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import Server.Coordinator.MedicoController;

public class InserisciPaziente extends JFrame {

	private JPanel contentPane;
	private JTextField nameField;
	private JTextField surnameField;
	private JTextField cfField;
	private JTextField sexField;
	private JTextField grupposanguignoField;
	private JTextField dipendenzeField;
	private JTextField dataField;
	private JTextField allergiaField;
	private JTextField residenzaField;
	private JTextField cellField;
	private JLabel grupposanguingoLabel;
	private JLabel dipendenzeLabel;
	private JLabel residenza;
	private JButton btnMenu;
	private JLabel allergiaLabel;
	private JLabel cellLabel;
	
	/**
	 * Launch the application.
	 */
	public static void inserisciPaziente() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					InserisciPaziente frame = new InserisciPaziente();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public InserisciPaziente() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 309);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnNewButton = new JButton("Inserisci");
		btnNewButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				boolean status = false;
				MedicoController m = MedicoController.getSingleton();
				String dataStr = dataField.getText();
				String cf = cfField.getText();
				cf = cf.toUpperCase();
				status = verificaCF(cf);
				if(status) {
					try {
						Date date= Date.valueOf(dataStr);
						
						status = m.inserisciPaziente(nameField.getText(), surnameField.getText(),cf,grupposanguignoField.getText(), allergiaField.getText(), dipendenzeField.getText(), date, cellField.getText(), sexField.getText().charAt(0), residenzaField.getText());
					}catch(Exception exc) {
						exc.printStackTrace();
						status = false;
					}
				}
				if(!status) {
					JOptionPane.showMessageDialog(contentPane, "Impossibile inserire utente, riprova!", "Attenzione", JOptionPane.ERROR_MESSAGE);
				}else {
					JOptionPane.showMessageDialog(contentPane, "Utente inserito correttamente", "Info", JOptionPane.INFORMATION_MESSAGE);
				}
			}
		});
		btnNewButton.setBounds(182, 242, 96, 21);
		contentPane.add(btnNewButton);
		
		nameField = new JTextField();
		nameField.setBounds(159, 0, 229, 19);
		contentPane.add(nameField);
		nameField.setColumns(10);
		
		surnameField = new JTextField();
		surnameField.setBounds(159, 22, 229, 19);
		contentPane.add(surnameField);
		surnameField.setColumns(10);
		
		cfField = new JTextField();
		cfField.setBounds(159, 44, 229, 19);
		contentPane.add(cfField);
		cfField.setColumns(10);
		
		sexField = new JTextField();
		sexField.setBounds(159, 64, 69, 19);
		contentPane.add(sexField);
		sexField.setColumns(10);
		
		grupposanguignoField = new JTextField();
		grupposanguignoField.setBounds(159, 113, 29, 20);
		contentPane.add(grupposanguignoField);
		grupposanguignoField.setColumns(10);
		
		dipendenzeField = new JTextField();
		dipendenzeField.setBounds(159, 143, 229, 19);
		contentPane.add(dipendenzeField);
		dipendenzeField.setColumns(10);
		
		residenzaField = new JTextField();
		residenzaField.setBounds(159, 84, 229, 19);
		contentPane.add(residenzaField);
		residenzaField.setColumns(10);
		
		
		JLabel nameLabel = new JLabel("nome");
		nameLabel.setBounds(40, 0, 45, 21);
		contentPane.add(nameLabel);
		
		JLabel surnameLabel = new JLabel("cognome");
		surnameLabel.setBounds(40, 26, 69, 13);
		contentPane.add(surnameLabel);
		
		JLabel cfLabel = new JLabel("CF");
		cfLabel.setBounds(40, 48, 45, 13);
		contentPane.add(cfLabel);
		
		JLabel sexLabel = new JLabel("sesso");
		sexLabel.setBounds(40, 67, 45, 13);
		contentPane.add(sexLabel);
		
		grupposanguingoLabel = new JLabel("gruppo sanguigno");
		grupposanguingoLabel.setBounds(40, 116, 109, 13);
		contentPane.add(grupposanguingoLabel);
		
		dipendenzeLabel = new JLabel("dipendenze");
		dipendenzeLabel.setBounds(40, 146, 76, 13);
		contentPane.add(dipendenzeLabel);
		
		residenza = new JLabel("residenza");
		residenza.setBounds(40, 87, 86, 13);
		contentPane.add(residenza);
		
		btnMenu = new JButton("Menu");
		btnMenu.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				Menu.start();
				CloseFrame();
			}
		});
		btnMenu.setBounds(292, 242, 96, 21);
		contentPane.add(btnMenu);
		
		dataField = new JTextField();
		dataField.setColumns(10);
		dataField.setBounds(312, 64, 76, 19);
		contentPane.add(dataField);
		
		allergiaField = new JTextField();
		allergiaField.setColumns(10);
		allergiaField.setBounds(159, 172, 229, 19);
		contentPane.add(allergiaField);
		
		JLabel lblNewLabel = new JLabel("data nascita");
		lblNewLabel.setBounds(248, 67, 76, 13);
		contentPane.add(lblNewLabel);
		
		allergiaLabel = new JLabel("allergie");
		allergiaLabel.setBounds(40, 177, 76, 13);
		contentPane.add(allergiaLabel);
		
		cellLabel = new JLabel("cellulare");
		cellLabel.setBounds(40, 208, 45, 13);
		contentPane.add(cellLabel);
		
		cellField = new JTextField();
		cellField.setColumns(10);
		cellField.setBounds(159, 201, 229, 19);
		contentPane.add(cellField);
	}
	
	private void CloseFrame() {
		super.dispose();
	}
	
	private boolean verificaCF(String cf) {
		if( cf.length()!=16 ) {
			return false;
		}
		int[] alpPos = {0,1,2,3,4,5,8,11,15};
		int[] numPos = {6,7,9,10,12,13,14};
		for(int i: alpPos) {
			if( !Character.isLetter(cf.charAt(i))) {
				return false;
			}
		}
		for(int i: numPos) {
			if( !Character.isDigit(cf.charAt(i))) {
				return false;
			}
		}
		String mesiValidi = "ABCDEHLMPRST";
		Character mese = cf.charAt(8);
		if(mesiValidi.indexOf(mese) == -1) {
			return false;
		}
		
		String alpOdd = "BAKPLCQDREVOSFTGUHMINJWZYX";
		String numOdd = "10   2 3 4   5 6 7 8 9    ";
		int checksum = 0;
		for(int i = 1; i < 15; i+=2) {
			//even
			if(Character.isDigit(cf.charAt(i))) {
				checksum += Character.getNumericValue(cf.charAt(i));
			}else {
				checksum += (int)cf.charAt(i) - (int)'A';
			}
		}
		for(int i = 0; i<15; i+=2) {
			//odd
			if(Character.isDigit(cf.charAt(i))) {
				checksum += numOdd.indexOf(cf.charAt(i));
			}else {
				checksum += alpOdd.indexOf(cf.charAt(i));
			}
		}
		checksum = checksum % 26;
		Character check = (char) (checksum + (int)'A');
		if(cf.charAt(15) != check) {
			return false;
		}
		
		return true;
	}
}
