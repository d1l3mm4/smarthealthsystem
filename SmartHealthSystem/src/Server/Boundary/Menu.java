package Server.Boundary;


import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;


public class Menu extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void start() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Menu frame = new Menu();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Menu() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 453, 325);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton inseriscipazienteButton = new JButton("Inserisci paziente");
		inseriscipazienteButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
			InserisciPaziente.inserisciPaziente();
			dispose();
			}
		});
		inseriscipazienteButton.setBounds(109, 24, 222, 29);
		contentPane.add(inseriscipazienteButton);
		
		JButton visualizzaListaPrenotazioniButton = new JButton("Visualizza lista prenotazioni");
		visualizzaListaPrenotazioniButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				VisualizzaPrenotazioni.getPrenotazioni();
				dispose();
			}
		});
		visualizzaListaPrenotazioniButton.setBounds(109, 238, 222, 29);
		contentPane.add(visualizzaListaPrenotazioniButton);
		
		JButton iniziaNuovaTerapiaButton = new JButton("Inizia nuova terapia");
		iniziaNuovaTerapiaButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				NuovaTerapia.inserisciNuovaTerapia();
				dispose();
			}
		});
		iniziaNuovaTerapiaButton.setBounds(109, 78, 222, 29);
		contentPane.add(iniziaNuovaTerapiaButton);
		
		JButton visualizzaPazientiInCuraButton = new JButton("Visualizza pazienti in cura");
		visualizzaPazientiInCuraButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				PazientiInCura.getPazientiInCura();
				dispose();
			}
		});
		visualizzaPazientiInCuraButton.setBounds(109, 133, 222, 29);
		contentPane.add(visualizzaPazientiInCuraButton);
		
		JButton ricercaPazienteButton = new JButton("Ricerca paziente");
		ricercaPazienteButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				RicercaPaziente.ricercaPaziente();
				dispose();
			}
		});
		ricercaPazienteButton.setBounds(109, 53, 222, 29);
		contentPane.add(ricercaPazienteButton);
		
		JButton terminaTerapiaButton = new JButton("Termina terapia");
		terminaTerapiaButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				TerminaTerapia.terminaTerapia();
				dispose();
			}
		});


		terminaTerapiaButton.setBounds(109, 105, 222, 29);
		contentPane.add(terminaTerapiaButton);
		
		JButton visualizzaCondizioniButton = new JButton("Visualizza condizioni attuali paziente");
		visualizzaCondizioniButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				CondizioniPaziente.visualizzaCondizioniPaziente();
				dispose();
			}
		});
		visualizzaCondizioniButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		visualizzaCondizioniButton.setBounds(109, 161, 222, 29);
		contentPane.add(visualizzaCondizioniButton);
		
		JButton visualizzaStoricoButton = new JButton("Visualizza storico paziente");
		visualizzaStoricoButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				StoricoPaziente.visualizzaStoricoPaziente();
			}
		});

		visualizzaStoricoButton.setBounds(109, 187, 222, 29);
		contentPane.add(visualizzaStoricoButton);
		
		JButton inserisciPrescrizioneButton = new JButton("Inserisci prescrizione medica");
		inserisciPrescrizioneButton.setVisible(false);
		inserisciPrescrizioneButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
			}
		});
		inserisciPrescrizioneButton.setBounds(109, 214, 222, 29);
		contentPane.add(inserisciPrescrizioneButton);
	}
	

}
