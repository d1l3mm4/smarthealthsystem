package Server.Boundary;


import java.awt.EventQueue;
import java.awt.FlowLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import Server.Coordinator.MedicoController;

import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class StoricoPaziente extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextArea jta;

	/**
	 * Launch the application.
	 */
	public static void visualizzaStoricoPaziente() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					StoricoPaziente frame = new StoricoPaziente();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public StoricoPaziente() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel CFlabel = new JLabel("CF:");
		CFlabel.setBounds(34, 31, 45, 13);
		contentPane.add(CFlabel);
		
		textField = new JTextField();
		textField.setBounds(89, 28, 172, 19);
		contentPane.add(textField);
		textField.setColumns(10);
		
		jta = new JTextArea(10, 20);
		
		JScrollPane scrollPane = new JScrollPane(jta, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		setLayout(new FlowLayout());
		
		scrollPane.setVisible(true);
		add(scrollPane);
		setLocationRelativeTo(null);
		JButton findButton = new JButton("visualizza storico");
		findButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				MedicoController m= MedicoController.getSingleton();
				jta.setText(m.visualizzaStorico(textField.getText()));
			}
		});
		findButton.setBounds(287, 27, 139, 21);
		contentPane.add(findButton);
		

		
		
		JButton menuButton = new JButton("menu");
		menuButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				Menu.start();
				dispose();
			}
		});
		menuButton.setBounds(176, 242, 85, 21);
		contentPane.add(menuButton);
		

		
	}
}
