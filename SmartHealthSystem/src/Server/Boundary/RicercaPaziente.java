package Server.Boundary;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import Server.Coordinator.MedicoController;

import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JTextArea;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class RicercaPaziente extends JFrame {

	private JPanel contentPane;
	private JTextField nameField;
	private JTextField surnameField;
	private JTextField textArea;

	/**
	 * Launch the application.
	 */
	public static void ricercaPaziente() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					RicercaPaziente frame = new RicercaPaziente();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public RicercaPaziente() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel nameLabel = new JLabel("nome:");
		nameLabel.setBounds(50, 20, 45, 13);
		contentPane.add(nameLabel);
		
		JLabel lblCognome = new JLabel("cognome:");
		lblCognome.setBounds(50, 43, 75, 13);
		contentPane.add(lblCognome);
		
		nameField = new JTextField();
		nameField.setBounds(130, 17, 145, 19);
		contentPane.add(nameField);
		nameField.setColumns(10);
		
		surnameField = new JTextField();
		surnameField.setColumns(10);
		surnameField.setBounds(130, 43, 145, 19);
		contentPane.add(surnameField);
		
		JTextArea textAreaa = new JTextArea();
		textAreaa.setEditable(false);
		textAreaa.setBounds(21, 85, 405, 134);
		contentPane.add(textAreaa);
		
		JButton findButton = new JButton("cerca");
		findButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				
				MedicoController m= MedicoController.getSingleton();
				String toDisplay = m.ricercaPaziente(nameField.getText(), surnameField.getText());
				System.out.println(toDisplay);
	
				if (toDisplay.isEmpty()) {
					textAreaa.setText("paziente non trovato");
				}
				else {
					textAreaa.setText(toDisplay);
				}
				
			}
		});
		findButton.setBounds(310, 20, 85, 36);
		contentPane.add(findButton);
		
		JButton menuButton = new JButton("menu");
		menuButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				Menu.start();
				dispose();
			}
		});
		menuButton.setBounds(190, 227, 85, 31);
		contentPane.add(menuButton);
		

		
	}
}
