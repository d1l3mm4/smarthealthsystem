package Server.Boundary;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.border.EmptyBorder;

import Server.Coordinator.MedicoController;

public class VisualizzaPrenotazioni extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void getPrenotazioni() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					VisualizzaPrenotazioni frame = new VisualizzaPrenotazioni();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public VisualizzaPrenotazioni() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
	     JTextArea area= new JTextArea(15, 30);
	        
	        JScrollPane scrollPane = new JScrollPane(area,
	            JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
	            JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
	        
	        getContentPane().setLayout(new FlowLayout());
	        
			setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);        
	        
	   
	       
	        getContentPane().add(scrollPane);
	        setSize(400,370);
	        setLocationRelativeTo(null);
	        setVisible(true);
	        
	        MedicoController m = MedicoController.getSingleton();
	        
	        String toDisplay = m.visualizzaListaPrenotazioni();
	        
	        if (toDisplay== null) {
	        	area.setText("non ci sono prenotazioni");
	        }
	        else {
	        	area.setText(toDisplay);
	        }
	        
	        JButton btnNewButton = new JButton("Menu");
	        btnNewButton.addMouseListener(new MouseAdapter() {
	        	@Override
	        	public void mouseClicked(MouseEvent e) {
	        		dispose();
	        		Menu.start();
	        	}
	        });
	        getContentPane().add(btnNewButton);
	}

}
