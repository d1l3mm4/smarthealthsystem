package Server.Proxy;
import Server.Coordinator.PazienteController;

import ProxyRMInterface.*;

public class ServerLoginProxy implements LoginProxy {
	@Override
	public boolean login(String CF,String password) {
	
		PazienteController controller= PazienteController.getSingleton();
		
		return controller.login(CF, password);
	}
	

}