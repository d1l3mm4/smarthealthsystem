package Server.Proxy;
import Server.Coordinator.PazienteController;
import java.util.List;
import java.sql.Timestamp;

import ProxyRMInterface.*;

public class ServerPrenotazioneProxy implements PrenotazioneProxy {
	@Override
	public List<Timestamp> richiediVisita(String CF, boolean scelta, int page, Timestamp time) {
	
		PazienteController controller= PazienteController.getSingleton();
		
		return controller.richiediVisita(CF, scelta, page, time);
	}
	

}