package Server.Proxy;
import Server.Coordinator.PazienteController;

import ProxyRMInterface.*;

public class ServerPrescrizioneProxy implements PrescrizioneProxy {
	@Override
	public String richiediPrescrizione(String CF) {
	
		PazienteController controller= PazienteController.getSingleton();
		
		return controller.visualizzaPrescrizioniMediche(CF);
	}
	

}