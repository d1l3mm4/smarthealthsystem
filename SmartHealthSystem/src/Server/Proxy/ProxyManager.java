package Server.Proxy;


import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

import ProxyRMInterface.LoginProxy;
import ProxyRMInterface.PrenotazioneProxy;
import ProxyRMInterface.PrescrizioneProxy;

public class ProxyManager {
	
	public void initProxy() {
		
		try {
			
			Registry registry = LocateRegistry.getRegistry("localhost");

			ServerPrenotazioneProxy proxy = new ServerPrenotazioneProxy();
			PrenotazioneProxy prenotazioneProxyI = (PrenotazioneProxy) UnicastRemoteObject.exportObject(proxy, 0);
			registry.rebind("prenotazione", prenotazioneProxyI);
			
			ServerLoginProxy proxy2 = new ServerLoginProxy();
			LoginProxy loginProxyI = (LoginProxy) UnicastRemoteObject.exportObject(proxy2, 0);
			registry.rebind("login", loginProxyI);
			
			ServerPrescrizioneProxy proxy3 = new ServerPrescrizioneProxy();
			PrescrizioneProxy prescrizioneProxyI = (PrescrizioneProxy) UnicastRemoteObject.exportObject(proxy3, 0);
			registry.rebind("prescrizione", prescrizioneProxyI);
			
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		
	}
}