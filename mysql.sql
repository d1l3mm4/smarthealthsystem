DROP TABLE IF EXISTS Pazienti;
DROP TABLE IF EXISTS Visite;
DROP TABLE IF EXISTS Terapie;
DROP TABLE IF EXISTS Misurazioni;
DROP TABLE IF EXISTS Prescrizioni;
DROP TABLE IF EXISTS Sintomi;
CREATE TABLE Pazienti (
  CF                 CHAR(16) NOT NULL, 
  nome               VARCHAR(255) NOT NULL, 
  cognome            VARCHAR(255) NOT NULL, 
  residenza          VARCHAR(255) NOT NULL, 
  gruppoSanguigno    VARCHAR(3) NOT NULL, 
  allergie           VARCHAR(255), 
  dipendenze         VARCHAR(255), 
  dataNascita        DATE NOT NULL, 
  recapitoTelefonico VARCHAR(20) NOT NULL, 
  dataMorte          DATE DEFAULT NULL, 
  causaMorte         VARCHAR(255), 
  password           VARCHAR(255) NOT NULL,
  CONSTRAINT PK_Pazienti PRIMARY KEY (CF)
);
CREATE TABLE Visite (
  ID          INT AUTO_INCREMENT, 
  data        DATETIME NOT NULL, 
  domiciliare BOOLEAN DEFAULT FALSE NOT NULL, 
  --conferma    BOOLEAN DEFAULT FALSE NOT NULL, 
  PazienteCF  CHAR(16),
  CONSTRAINT PK_Visite PRIMARY KEY (ID)
);
CREATE TABLE Terapie (
  ID         INT NOT NULL AUTO_INCREMENT, 
  dataInizio DATE NOT NULL, 
  dataFine   DATE DEFAULT NULL, 
  diagnosi   VARCHAR(255), 
  PazienteCF CHAR(16) NOT NULL,
  prescrizioni varchar(255) DEFAULT NULL,
  CONSTRAINT PK_Terapie PRIMARY KEY (ID)
);
CREATE TABLE Misurazioni (
  ID              INT NOT NULL AUTO_INCREMENT, 
  TerapiaID       INT NOT NULL, 
  orario       TIMESTAMP DEFAULT NOW() NOT NULL, 
  tipoMisurazione CHAR(1) NOT NULL, 
  valore          FLOAT NOT NULL, 
  valoreMassimo   FLOAT,
  CONSTRAINT PK_Misurazioni PRIMARY KEY (ID)
);
CREATE TABLE Prescrizioni (
  ID         INT NOT NULL AUTO_INCREMENT, 
  TerapiaID  INT, 
  nome       VARCHAR(255) NOT NULL, 
  isFarmaco  BOOLEAN DEFAULT FALSE NOT NULL, 
  dataInizio DATE, 
  dataFine   DATE, 
  frequenza  INT,
  CONSTRAINT PK_Prescrizioni PRIMARY KEY (ID)
);

CREATE TABLE Sintomi (
  ID        INT NOT NULL AUTO_INCREMENT, 
  TerapiaID INT, 
  orario TIMESTAMP DEFAULT NOW() NOT NULL, 
  nome      VARCHAR(255) NOT NULL,
  CONSTRAINT PK_Sintomi PRIMARY KEY (ID)
);

ALTER TABLE Visite ADD CONSTRAINT richiede FOREIGN KEY (PazienteCF) REFERENCES Pazienti (CF);
ALTER TABLE Terapie ADD CONSTRAINT FKTerapie826500 FOREIGN KEY (PazienteCF) REFERENCES Pazienti (CF);
ALTER TABLE Misurazioni ADD CONSTRAINT FKMisurazion753274 FOREIGN KEY (TerapiaID) REFERENCES Terapie (ID);
ALTER TABLE Prescrizioni ADD CONSTRAINT FKPrescrizio17013 FOREIGN KEY (TerapiaID) REFERENCES Terapie (ID);
ALTER TABLE Sintomi ADD CONSTRAINT FKSintomi910749 FOREIGN KEY (TerapiaID) REFERENCES Terapie (ID);


COMMIT;

DROP PROCEDURE IF EXISTS make_intervals;

DELIMITER //
 
CREATE PROCEDURE make_intervals(startdate timestamp, enddate timestamp, intval integer, unitval varchar(10))
BEGIN
-- *************************************************************************
-- Procedure: make_intervals()
--    Author: Ron Savage
--      Date: 02/03/2009
--
-- Description:
-- This procedure creates a temporary table named time_intervals with the
-- interval_start and interval_end fields specifed from the startdate and
-- enddate arguments, at intervals of intval (unitval) size.
-- *************************************************************************
   DECLARE thisDate TIMESTAMP;
   DECLARE nextDate TIMESTAMP;
   SET thisDate = startdate;
 
   -- *************************************************************************
   -- Drop / create the temp table
   -- *************************************************************************
   DROP TABLE IF EXISTS PSSS_Schema.time_intervals;
   CREATE TABLE IF NOT EXISTS PSSS_Schema.time_intervals
      (
      interval_start TIMESTAMP
      );
 
   -- *************************************************************************
   -- Loop through the startdate adding each intval interval until enddate
   -- *************************************************************************
   REPEAT
      SELECT
         case unitval
            when 'MICROSECOND' then timestampadd(MICROSECOND, intval, thisDate)
            when 'SECOND'      then timestampadd(SECOND, intval, thisDate)
            when 'MINUTE'      then timestampadd(MINUTE, intval, thisDate)
            when 'HOUR'        then timestampadd(HOUR, intval, thisDate)
            when 'DAY'         then timestampadd(DAY, intval, thisDate)
            when 'WEEK'        then timestampadd(WEEK, intval, thisDate)
            when 'MONTH'       then timestampadd(MONTH, intval, thisDate)
            when 'QUARTER'     then timestampadd(QUARTER, intval, thisDate)
            when 'YEAR'        then timestampadd(YEAR, intval, thisDate)
         end into nextDate;
      IF (WEEKDAY(thisDate) != 6) AND ((HOUR(thisDate) >= 9 AND HOUR(thisDate) < 13) OR (HOUR(thisDate) >= 14 AND HOUR(thisDate) < 18)) THEN
		INSERT INTO time_intervals SELECT thisDate;
      END IF;
      SET thisDate = nextDate;
   UNTIL thisDate >= enddate
   END REPEAT;
 
 END
 //
 DELIMITER ;


CREATE EVENT genera_time_intervals
  ON SCHEDULE
    EVERY 1 DAY
    STARTS (TIMESTAMP(CURRENT_DATE) + INTERVAL 1 DAY + INTERVAL 1 HOUR)
  DO
    CALL make_intervals(curdate(), adddate(curdate(), 15), 1, 'HOUR');
  

COMMIT;